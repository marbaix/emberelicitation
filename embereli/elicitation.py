# -*- coding: utf-8 -*-
"""
Elicitation is a work in progress - technical details may change, and part of the documentation is missing.
It is likely that part of this code will be merged with EmberFactory.makember, which it currently duplicates.
(however, Emberfactory.ember is not duplicated, and is entirely used here as provided in the EF)

Copyright (C) 2021  philippe.marbaix@uclouvain.be
"""
import numpy as np
import random
import os
import sys
from reportlab.lib import units
from embermaker import helpers as hlp
from embermaker.embergraph import EmberGraph
from embermaker import readembers
from embermaker import parameters as param
from embereli import probadist as pdist


def elicitation(inpath=None, outfile=None, pdname='standard', prefcsys='RGB', logger=None):
    """
    Reads ember data from .xlsx files from several experts, provides a summary diagram...
    Work in progress (07/2021)

    :param inpath: The name of the data file (.xlsx) or directory. Mandatory.
    :param outfile: An optional name for the output file; default = path and name of inpath, extension replaced to .pdf
    :param prefcsys: The prefered color system (also called mode): RGB or CMYK.
    :return: a list of two strings: [output file path, error message if any]
    """

    # Input file:
    if inpath is None:
        return {'error': logger.addfail("No input file.")}

    # Get graph parameters
    # --------------------
    gp = param.ParamDict()

    # Add graphic parameter for the elicitation desk:
    # (the format is not trivial but that is because we 'supplement' the reading from the EF = from md file)
    # we might have a paramdefs.md file here, so this hard-coding would no longer be needed.
    if pdname == 'standard':
        pdname = 'm-PERT'

    # Adjust default parameters
    # -------------------------
    gp['max_gr_line'] = 2
    gp['be_top_y'] = 1*units.cm
    gp['be_int_x'] = 1.1*units.cm
    gp['gr_int_x'] = 1*units.cm
    gp['be_x'] = 0.6 * units.cm
    gp['leg_y'] = 0.4 * units.cm
    gp['fnt_size'] = 9


    # Read the ember-data sheets
    # ----------------------------
    pfiles = [os.path.join(inpath, file) for file in os.listdir(inpath) if os.path.splitext(file)[-1] == '.xlsx'
              and os.path.splitext(file)[0][0] != '~']

    lbes = []
    for pfile in pfiles:
        wbmain = hlp.secure_open_workbook(pfile)
        try:
            sht = wbmain["Assessment"]  # Todo: revert to using this sht for reading below, when EmberMaker is > 2.0.1
        except KeyError:
            return {'error': logger.addfail("Invalid input file: no sheet named 'Assessment' was found")}

        # Reset valid_range, because some files may define a range while others do not:
        # at present, the result of this is that the files which sets the range defines it for the others.
        # the result is unpredictable because the order of files is not defined => we reset the default.
        # This could be improved but would require a global reflexion about how be.valid_range is read from files
        gp['haz_valid_top'] = 5.0
        gp['haz_valid_bottom'] = 0.0

        # Read ember(s) from file
        rembers = readembers.embers_from_xl(wbmain, gp=gp, logger=logger)

        # Abort if there is an error or no data
        if 'error' in rembers:
            return {'error': logger.addfail("Invalid input file: could not read ember data")}
        if len(rembers[0].trans) == 0:
            return {'error': logger.addfail(f"A file does not seem to contain assessment "
                                            f"data: '{os.path.basename(pfile)}'")}
        lbes = lbes + rembers

    # Adjust global range parameters and hazard variable (minimal solution for future improvement)
    gp['haz_axis_top'] = max([be.haz_valid[1] for be in lbes])
    gp['haz_axis_bottom'] = min([be.haz_valid[0] for be in lbes])
    if gp['haz_name_std'] not in ['GMT', 'GMST']:  # Temporary solution, waiting for a global handling of name_std
        gp['haz_name'] = ''  # EF default is global mean temperature.

    # Create the ember graph
    # ----------------------
    if outfile is None:
        outfile = (os.path.splitext(pfiles[0])[0] + '.pdf').replace('/in/', '/out/')
        # Todo: this is temporary, a more relevant file name needs to be generated.

    egr = EmberGraph(outfile, gp=gp, grformat="PDF", logger=logger)
    # logger.addinfo(f"Graphical parameters", mestype='title')
    # logger.addinfo(str(gp))  # In principle not useful for this app - would re-insert if it proves useful.

    # Add groups of embers (and the embers it contains), to the graph
    # ---------------------------------------------------------------
    egr.add(lbes)

    # Probadist - elicitation
    # -----------------------
    pdist.probadist(lbes, egr, pdname, logger)

    # Actually produce the diagram
    # ----------------------------
    logger.addinfo("Drawing embers", mestype='title')
    outfile = egr.draw()

    # Clone of the former output, which is still needed (should be simplified in future versions)
    odict = {'outfile': outfile, 'width': str(egr.cx.b)}
    critical = logger.getlog("CRITICAL")
    if len(critical) > 0:
        odict["error"] = ", ".join(critical)  # Legacy appraoch: 'error' actually stops the processing (see control.py)
    return odict

if __name__ == "__main__":
    plotresult = elicitation(sys.argv[1])
    os.popen("open '" + plotresult['outfile'] + "'")
