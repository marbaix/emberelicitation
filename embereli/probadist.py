from scipy.stats import beta, triang, rv_continuous
from embermaker.lineplot import LinePlot, Line
import numpy as np

class rv_step(rv_continuous):
    """
    'Step function' random variable: constant PDF under and above median
    Only for experimental purposes at the moment
    """
    def __init__(self, bot=0.0, med = None, top= 1.0):
        super().__init__(self, a=bot, b=top)
        self.med = med if med is not None else (bot + top) / 2.0
        if self.med >= top or self.med <= bot:
            raise Exception ("Bad parameters: {} {} {}".format(bot, med, top))
        self.x1 = 0.5 / (self.med - bot)
        self.x2 = 0.5 / (top - self.med)

    def _cdf(self, x):
       y = np.piecewise(x, [x < self.med, x >= self.med],
                             [lambda x: self.x1 * (x-self.a),
                              lambda x: self.x2 * (x-self.med) + 0.5])
       return y


class rv_aggregate(rv_continuous):
    """
    Generates an 'aggregated' random variable from received random variables
    """
    def __init__(self, **kwargs):
        super().__init__(self, **kwargs)
        self.members=[]

    def append(self,rv,bot=None,top=None):
        """
        Adds a random variable to a rv_aggregate object. Extends the support of the rv_aggregate as needed.
        :param rv: the random variable
        :param bot: optional; replaces the lower bound of the support of the added rv
        :param top: optional; replaces the upper bound of the support of the added rv
        """
        nbot = rv.a if bot is None else bot
        ntop = rv.b if top is None else top
        if len(self.members)==0:
            self.a = nbot
            self.b = ntop
        else:
            self.a = min(self.a, nbot)
            self.b = max(self.b, ntop)

        self.members.append(rv)

    def isempty(self):
        return len(self.members) == 0

    def _cdf(self, x):
        y = 0
        nrvs = len(self.members)
        for rv in self.members:
            y += rv.cdf(x)
        return (y / nrvs)

def probadist(lbes, egr, pdname, logger):
    logger.addinfo(f"Aggregated probability section", mestype="title")

    # Todo: fix this - the values should be dynamic!
    gbot, gtop = egr.gp['haz_axis_bottom'], egr.gp['haz_axis_top']

    # Create a lineplot with the EmberMaker lib - easier for integration with ember diagrams
    lp = LinePlot(egr, xaxis_name="Probability density (%/°C)", title="Aggregated probability")

    x = np.linspace(gbot, gtop, 200)
    # Todo: this is (somewhat) obsolete - Transition colours are now in colourPalette,
    #  but colours for elicitation are not there yet; to reconsider if 'non-standard' transitions are added.
    transcol = {'undetectable to moderate': 'dyellow',
                'moderate to high': 'red',
                'high to very high': 'dpurple'}

    # Assess transitions to higher risk levels only:
    transnames = [tr for tr in egr.cpal.transnames_risk if egr.cpal.transnames_risk[tr][1] > 0]
    for transname in transnames:
        arv = rv_aggregate()  # arv is random variable synthesising experts' input for this transition (transname)
        for be in lbes:
            if transname not in be.transnames():
                # This expert provided no data for that transition (=> skip)
                continue
            trans = be.transbyname(transname)
            tmin = be.gethazl(transname, 'min')
            tmax = be.gethazl(transname, 'max')
            scale = tmax-tmin

            if trans.mode is not None:
                # Expert provided a mode (most-likely hazard level for this transition)
                tmid = trans.mode
            else:
                # Expert provided a median or no mid-point at all.
                if 'median' in trans:
                    tmid = trans['median']
                else:
                    tmid = (trans['max'] + trans['min']) / 2.0
            # Reduce the provided mid-point to the standardized form in [0,1]:
            rmid = (tmid-tmin)/scale

            if pdname == 'm-PERT':
                # Modified PERT distribution
                gam = 2
                rmu = ((gam + 4.0/3.0) * rmid + 1.0/3.0) / (gam + 2.0)
                if trans.mode is None:
                    # for median
                    a = (gam + 2) * rmu
                    b = (gam + 2) * (1-rmu)
                else:
                    # for mode
                    a =  1 + gam * rmu
                    b =  1 + gam * (1-rmu)
                rv = beta(a, b, loc=tmin, scale=scale)

            elif pdname == 'triang':
                # Triangular distribution
                if trans.mode is None:
                    # for median
                    if rmid > 0.5:
                        rmod = 2 * rmid**2.0
                    else:
                        rmod = 1 - 2 * (1 - rmid) ** 2
                    rmod = min(1.0, max(0, rmod)) # limit to [0,1]
                    rv = triang(rmod, loc=tmin, scale=scale)
                else:
                    # for mode
                    rv = triang(rmid, loc=tmin, scale=scale)

            else:
                raise ValueError("Unknown probability distribution")

            #lp.data.append((tuple(zip(rv.pdf(x), x))))

            arv.append(rv, bot = tmin, top = tmax)
            median = rv.median()
            if 'median' not in trans.phases():
                trans.append_level('median', median)
                trans.levels.sort(key=lambda lev : lev["hazl"])
                # levels are an ordered list, but append_level is adding at the end, as expected given its name.
                # Should the change in the future?

            #lp.data.append(((0.0,median),(0.4,median)))  # Was used to show individual median on the graph
            #logger.addinfo ('tmid: ', tmid, ' Median: ', median)  # Verification?

            # thazl = [level['hazl'] for level in trans.levels]
            #logger.addinfo(f"Ember {be.name} - {trans.name} : {thazl}")

        # If no expert provided data for a given transition, do not try to plot result
        if arv.isempty():
            continue

        median = arv.median()
        logger.addinfo(f"Median for '{trans.name}': {median}")

        color = transcol[transname]
        lp.add(Line(xs=arv.pdf(x)*100.0, ys=x, color=color))

    return