The Ember Elicitaiton Desk
==========================

Objective
---------
The purpose of this software is to help assessing and illustrating climate change risks within a structured
expert elicitation process in view of building "burning ember" figures.

This application and the related template are based on experience gained through expert elicitation during
past IPCC reports, as described in <a href="https://www.nature.com/articles/s43017-020-0088-0">Zommers et al. 2020</a>
(section 'Methods in the SRCCL' and figure 5).

Several people contributed to discussions which helped in creating this app and its template file.
We thank them all, and may provide a list of names in future versions.

Development history
-------------------
This app was drafted in 2021. It is made public in the hope that it might help new scientific assessments.
If any important issue(s) surfaces, we would make all possible efforts to solve them. While we expect this tool
to be reliable, it has a relatively limited set of features that might be broadened, both with regard to the extent
to which it helps expert elicitation and to its technical foundation (especially the user interface, which could be 
brought in line with the design of the [EmberFactory](climrisk.org/emberfactory)).

Feedback would be much appreciated, not least because it would help us define priorities.