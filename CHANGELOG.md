# Changelog

Todo:
- further improve the template and documentation.
- ** provide a compilation of text provided by experts, such as the justification for each transition **
- provide options, perhaps in the form of a "facilitator file", similar to the options in the EF (eg. name of the
  hazard axis, to enable ocean pH as a substitute for GMT, etc.)
- change or remove the warning about overlapping transitions? (it is normal to have such warnings at elicitation stage)
- re-check how files are stored on the server (hopefully ok but better to check)

## [0.5b] - April 2024

Bug fixes and upgraded UI and template.

## [0.3b] - January 2024

First public release, labelled 'beta' while waiting for feedback, etc.

### Changes
- the drawing is done by the EmberMaker library (released at the end of 2023), providing full consistency between the
  embers and the 'probability' x-y plot.
- file selection is improved.

## [0.2] - 29 August 2021

### Additions
- a simple anonymisation mechanism is added: the numbering (and order or presentation) of experts is now random
- development of the Tutorial

### Changes
- Bug fix: "keep my files on the server" only kept one, now it keeps all.

## [0.1] - 2021

- First incomplete version
- There is some code duplication between elicitation.py and the EmberFactory