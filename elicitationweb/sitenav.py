# -*- coding: utf-8 -*-
"""
Site navigation: manages the pages that are not directly related to ember production
"""
from flask import Blueprint
from flask import render_template
from flask import url_for, redirect
from flask import current_app
from flask import request
from flask import send_from_directory
from flask import session
from os import path
from markdown import markdown
from emberfactory import helpers as webhlp
from embermaker import __init__ as emkinit

bp = Blueprint("sitenav", __name__)

@bp.route("/index")
def index():
    if "reset" in request.args:
        session["result-msg"]=None

    # If a result is available, move to the result page
    if "result-msg" in session and session["result-msg"] is not None \
            and "outfile_pdf" in session and path.isfile(session['outfile_pdf']):
        return redirect(url_for('control.result'))

    # If there is no result available, show the start page
    else:
        settings = {'checked_triang': '', 'checked_standard': ''}
        if current_app.config['UI_PREFERRED_PDIST'] == 'triang':
            settings['checked_triang'] = 'checked'
        else:
            settings['checked_standard'] = 'checked'  # Default and fallback if config is mistyped
        return render_template("elicitationweb/start.html", settings=settings)

# Enable downloading examples:
@bp.route('/doc/<path:filepath>', methods=['GET', 'POST'])
def examples_fil(filepath):
    examples_dir = path.join(current_app.root_path, "doc/")
    return send_from_directory(examples_dir, filepath)

# Load the 'doc' pages (examples and documentation about parameters etc.)
# Doc pages are written in markdown and included in the doc/ folder.
@bp.route('/doc/<subpage>', methods=['GET', 'POST'])
@bp.route('/doc/', methods=['GET', 'POST'])
def doc(subpage='tutorial'):
    doc_page = path.join(current_app.root_path, 'doc/'+subpage+'.md')
    try:
        with open(doc_page, "r", encoding="utf-8") as input_file:
            content = input_file.read()
    except FileNotFoundError:
        return render_template("elicitationweb/error_snip.html", message={"error":"Could not handle request"})

    # Add support for a specific-type of file inclusion
    addsource = path.join(path.dirname(emkinit.__file__), 'defaults/paramdefs.md')  # 'addsource' comes from EmberMaker!
    content = webhlp.mdcombine(content, addsource)
    # Process markdown
    content = markdown(content, extensions=['tables'])
    # Add support for colspan when empty cells are found
    content = webhlp.colspan(content)
    # Todo: revise/simplify using code from EmberFactory (2024)?
    #Add code to transform links to buttons when at the beginning of a paragraph
    txbutton = 'class="btn btn-right" > <svg class="btn-icon" aria-hidden="dtrue">\
        <use href=' + url_for('static', filename='icons.svg') + '#download></use></svg>'
    txmarker = '<p><a href'
    thesplit = content.split(txmarker)
    content = thesplit[0]
    for txfrag in thesplit[1:]:
        content += txmarker + txfrag.replace('>', txbutton, 1)
    content = content.replace(' $class=', '"class="')   # Todo: revise & improve introduction of classes.
    content = content.replace('<!--boxstart-->', '<div class="txtbox">').replace('<!--boxend-->', '</div>')

    return render_template("elicitationweb/doc.html", content=content, pagename=subpage)


# Load the 'More information' page
@bp.route('/more')
def more():
    return render_template("elicitationweb/more.html")
