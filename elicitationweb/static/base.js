/* Sticky navbar: add sticky class when needed */
window.onscroll = function () {
    stickyHelper()
};
let navbar = document.getElementsByClassName("navbar")[0];
let navOffset = navbar.offsetTop;

function stickyHelper() {
    if (window.scrollY > navOffset) {
        navbar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
    }
}

/* Responsive navbar: for small screens, add dropdown class to toggle menu presentation */
function navbarDrop() {
    if (navbar.classList.contains("dropdown")) {
        navbar.className = "navbar";
    } else {
        navbar.className += " dropdown";
    }
}

/* moveto page, intended at least for the error page */
function moveto(dest) {
    if (dest) {
        window.location.href = dest;
    } else {
        window.location.href = sindex;
    }
}
