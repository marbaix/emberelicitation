// Downloads and toggle visibility of log information
function logToggle(logurl) {
    const elog = document.getElementById("log");
    if (elog.classList.contains("hidden")) {
        elog.classList.remove("hidden");
        if (elog.childNodes.length === 0) {
            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    elog.innerHTML = this.responseText;
                }
            };
            xhttp.open("GET", logurl, true);
            xhttp.send();
        }
    } else {
        elog.classList.add("hidden");
    }
}
