// Larger image popup on click over image
const popup = document.getElementById("popup");
const popimg = document.getElementById("popimg");
const popables = document.getElementsByClassName("popable")

for (let i = 0; i < popables.length; i++) {
    popables[i].onclick = function() {
        popup.style.display = "block";
        popimg.src = this.src;
        console.debug(this.src);
    }
}

//Close button
const popclose = document.getElementById("popclose");
popclose.onclick = function() {
    popup.style.display = "none";
    console.debug("unpopped!");
}
