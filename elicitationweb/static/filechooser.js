/* Manages the file selection table & upload the content of the process form */

const fileSelect = document.getElementById("fileSelect")
const fileInput = document.getElementById("fileInput");
const form = document.getElementById("form")
var fileList = {}
var fileid = 0

fileSelect.addEventListener("click", function () {
    if (fileInput) {
        fileInput.click();
    }
}, false);

// Add file to our 'internal' list
function fileAdd(selectedFiles) {
    if (selectedFiles.length >= 0) {
        for (let i = 0; i < selectedFiles.length; i++) {
            fileList[`file${fileid}`] = selectedFiles[i];
            fileid += 1;
        }
        fileDisp()
    } else {
        console.debug("No files added");
    }
}

// Update the view
function fileDisp() {
    const fileNames = document.getElementById("fileNames");
    fileSelect.classList.add("selected");
    if (Object.entries(fileList).length === 0) {
        fileNames.innerHTML = "<tr><td style='color: grey'>No selected files yet</td></tr>";
    } else {
        var fileInfo = '';
        for (const [fid, file] of Object.entries(fileList)) {
            fileInfo += `<tr>
                <td>${file.name}</td>
                <td><button onclick="removeFile('${fid}')" style="border: none; background-color: transparent; color: #A00">Remove</button></td>
                </tr>\n`;
        }
        fileNames.innerHTML = fileInfo;
    }
}

fileDisp()  // initialise

function removeFile(fid) {
    delete fileList[fid]
    fileDisp()
}

// Function to POST the form's data on upload (Makes the code below more readable but fetch could be used directly)
async function postFData(data, csrf) {
     return await fetch(urlPOST, {
        method: "POST",
        redirect: "follow",
        headers: {
            'X-CSRF-TOKEN': csrf,
        },
        body: data,
    });
}

// Upload button
const uploadBtn = document.getElementById("uploadBtn");
uploadBtn.addEventListener("click", uploadFiles);

function uploadFiles(event) {
    event.preventDefault();
    if (fileList.length === 0) {
        alert("Please select at least one file to upload.");
        return;
    }
    const formData = new FormData(form);
    for (const [fid, file] of Object.entries(fileList)) {
        formData.append(`file`, file);
    }
    postFData(formData, csrf).then(response => {
        console.debug(response.ok);
        if (response.ok) {
            console.debug('Files uploaded successfully!');
            if (response.redirected) {
                // Have the browser actually follow the redirect
                window.location.href = response.url
            } else {
                // For the error page, Flask control.py only renders the content part of the page:
                // https://flask.palletsprojects.com/en/3.0.x/patterns/javascript/#replacing-content
                response.text().then(text => window.document.getElementById("content").innerHTML = text).catch(
                    (error) => {console.error(`An error occured while trying to redirect to the Results page: ${error}`);
    });
            }
        } else {
            alert("Error occurred during file upload. Please try again.");
        }
    }).catch( (error) => {
        console.error(`Posting files or data failed: ${error}`);
    });
}